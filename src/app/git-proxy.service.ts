import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/of';

const apiBasePath = "https://gitee.com/api";
const releaseTree = {
  "sha": "49bdea65698f050cdac65aac1fd7b8666325e471",
  "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/trees/49bdea65698f050cdac65aac1fd7b8666325e471",
  "tree": [
      {
          "path": ".gitignore",
          "mode": "100644",
          "type": "blob",
          "sha": "1eae0cf6700cebfee743504d0bdeaf642a11f012",
          "size": 20,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/1eae0cf6700cebfee743504d0bdeaf642a11f012"
      },
      {
          "path": "0.e1f45700e771b25d75be.chunk.js",
          "mode": "100644",
          "type": "blob",
          "sha": "1fc1b0af107acc175de853cfd4e28452401d4544",
          "size": 38686,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/1fc1b0af107acc175de853cfd4e28452401d4544"
      },
      {
          "path": "1.9d87db17c7b2c4c70121.chunk.js",
          "mode": "100644",
          "type": "blob",
          "sha": "74c44806b6c5cb558cb8cab9be3096947e840605",
          "size": 121737,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/74c44806b6c5cb558cb8cab9be3096947e840605"
      },
      {
          "path": "3rdpartylicenses.txt",
          "mode": "100644",
          "type": "blob",
          "sha": "9695bc6a27bcf79e1e92cdf513aa9bc6c511e3ab",
          "size": 7953,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/9695bc6a27bcf79e1e92cdf513aa9bc6c511e3ab"
      },
      {
          "path": "LICENSE",
          "mode": "100644",
          "type": "blob",
          "sha": "feca52f30f45f4611c2c7a06cc6d14394de17cb4",
          "size": 34548,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/feca52f30f45f4611c2c7a06cc6d14394de17cb4"
      },
      {
          "path": "README.md",
          "mode": "100644",
          "type": "blob",
          "sha": "cd7cfb3ab70496524b2ea84e87826e712464c3a3",
          "size": 62,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/cd7cfb3ab70496524b2ea84e87826e712464c3a3"
      },
      {
          "path": "assets",
          "mode": "040000",
          "type": "tree",
          "sha": "8ea868e2cdd6b36c4f962d6e38c7f754ab4abbdd",
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/trees/8ea868e2cdd6b36c4f962d6e38c7f754ab4abbdd"
      },
      {
          "path": "assets/favicons",
          "mode": "040000",
          "type": "tree",
          "sha": "d4894f835671605d56acf7c6197fbdd7439fa78b",
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/trees/d4894f835671605d56acf7c6197fbdd7439fa78b"
      },
      {
          "path": "assets/favicons/android-chrome-192x192.png",
          "mode": "100644",
          "type": "blob",
          "sha": "f13ea614535dc592a8f8ee80b3ea8e235e759ac8",
          "size": 5570,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/f13ea614535dc592a8f8ee80b3ea8e235e759ac8"
      },
      {
          "path": "assets/favicons/android-chrome-512x512.png",
          "mode": "100644",
          "type": "blob",
          "sha": "2332185483ea554738ac185297e8453cbf3fdad3",
          "size": 15886,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/2332185483ea554738ac185297e8453cbf3fdad3"
      },
      {
          "path": "assets/favicons/apple-touch-icon-114x114.png",
          "mode": "100644",
          "type": "blob",
          "sha": "31c26b22d22b288f97ce553a038083150718d80f",
          "size": 3227,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/31c26b22d22b288f97ce553a038083150718d80f"
      },
      {
          "path": "assets/favicons/apple-touch-icon-120x120.png",
          "mode": "100644",
          "type": "blob",
          "sha": "af73afb2aa9be826ae414100199d475ee6dc3da5",
          "size": 3340,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/af73afb2aa9be826ae414100199d475ee6dc3da5"
      },
      {
          "path": "assets/favicons/apple-touch-icon-144x144.png",
          "mode": "100644",
          "type": "blob",
          "sha": "3d1b3f9ff261a61913ad7466391be6dcaf388a36",
          "size": 3923,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/3d1b3f9ff261a61913ad7466391be6dcaf388a36"
      },
      {
          "path": "assets/favicons/apple-touch-icon-152x152.png",
          "mode": "100644",
          "type": "blob",
          "sha": "639271986868f1a01849df8ef6d4645c27d88b6a",
          "size": 3872,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/639271986868f1a01849df8ef6d4645c27d88b6a"
      },
      {
          "path": "assets/favicons/apple-touch-icon-180x180.png",
          "mode": "100644",
          "type": "blob",
          "sha": "7b51df7da33e62a58c978c1309fb40aa98ad774e",
          "size": 4873,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/7b51df7da33e62a58c978c1309fb40aa98ad774e"
      },
      {
          "path": "assets/favicons/apple-touch-icon-57x57.png",
          "mode": "100644",
          "type": "blob",
          "sha": "efc2f5774a201b76074e6929dd563a3ec22f5d2d",
          "size": 1475,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/efc2f5774a201b76074e6929dd563a3ec22f5d2d"
      },
      {
          "path": "assets/favicons/apple-touch-icon-60x60.png",
          "mode": "100644",
          "type": "blob",
          "sha": "7323ec69600038114106ee0bd1ec8b6620e2e4a6",
          "size": 1610,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/7323ec69600038114106ee0bd1ec8b6620e2e4a6"
      },
      {
          "path": "assets/favicons/apple-touch-icon-72x72.png",
          "mode": "100644",
          "type": "blob",
          "sha": "3b87ec1a10b2726244228afd3b7427c54e693eda",
          "size": 1719,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/3b87ec1a10b2726244228afd3b7427c54e693eda"
      },
      {
          "path": "assets/favicons/apple-touch-icon-76x76.png",
          "mode": "100644",
          "type": "blob",
          "sha": "f16058b16912ef52b94fb9aae4d8cff75de36fa9",
          "size": 1901,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/f16058b16912ef52b94fb9aae4d8cff75de36fa9"
      },
      {
          "path": "assets/favicons/apple-touch-icon.png",
          "mode": "100644",
          "type": "blob",
          "sha": "266ba2331d03e5506b949782fe91822b5f1e3a1c",
          "size": 4873,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/266ba2331d03e5506b949782fe91822b5f1e3a1c"
      },
      {
          "path": "assets/favicons/browserconfig.xml",
          "mode": "100644",
          "type": "blob",
          "sha": "f9c2e67fe6a04c5aa35d5d0be293f003c2f7e7da",
          "size": 246,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/f9c2e67fe6a04c5aa35d5d0be293f003c2f7e7da"
      },
      {
          "path": "assets/favicons/favicon-16x16.png",
          "mode": "100644",
          "type": "blob",
          "sha": "368c40cf689d09d67a575ed04a89abe613c66cf4",
          "size": 845,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/368c40cf689d09d67a575ed04a89abe613c66cf4"
      },
      {
          "path": "assets/favicons/favicon-32x32.png",
          "mode": "100644",
          "type": "blob",
          "sha": "824e033e7f4776de20fca243be35ceb5e3e23567",
          "size": 1247,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/824e033e7f4776de20fca243be35ceb5e3e23567"
      },
      {
          "path": "assets/favicons/favicon.ico",
          "mode": "100644",
          "type": "blob",
          "sha": "982e3106f2ff052ec1a1ed0e02af016d833bd6d5",
          "size": 15086,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/982e3106f2ff052ec1a1ed0e02af016d833bd6d5"
      },
      {
          "path": "assets/favicons/manifest.json",
          "mode": "100644",
          "type": "blob",
          "sha": "eaac9da74e6becf3ac4a2447094265177b986713",
          "size": 492,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/eaac9da74e6becf3ac4a2447094265177b986713"
      },
      {
          "path": "assets/favicons/mstile-144x144.png",
          "mode": "100644",
          "type": "blob",
          "sha": "b5a0b8112e8107fbf304d588ca999d87f3cf4c3b",
          "size": 4044,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/b5a0b8112e8107fbf304d588ca999d87f3cf4c3b"
      },
      {
          "path": "assets/favicons/mstile-150x150.png",
          "mode": "100644",
          "type": "blob",
          "sha": "9c3eb8ef9c09f424feed3cd9d4f1c6a3b40acde6",
          "size": 4133,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/9c3eb8ef9c09f424feed3cd9d4f1c6a3b40acde6"
      },
      {
          "path": "assets/favicons/mstile-310x150.png",
          "mode": "100644",
          "type": "blob",
          "sha": "b131032d728e00bd94bdc71450df9744d95a54b1",
          "size": 4626,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/b131032d728e00bd94bdc71450df9744d95a54b1"
      },
      {
          "path": "assets/favicons/mstile-310x310.png",
          "mode": "100644",
          "type": "blob",
          "sha": "6c290cfacb5270d3e8b0105f6974341f6f4c0736",
          "size": 8968,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/6c290cfacb5270d3e8b0105f6974341f6f4c0736"
      },
      {
          "path": "assets/favicons/mstile-70x70.png",
          "mode": "100644",
          "type": "blob",
          "sha": "c39938a55ac7cde50c847018235813a431528d74",
          "size": 2975,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/c39938a55ac7cde50c847018235813a431528d74"
      },
      {
          "path": "assets/favicons/safari-pinned-tab.svg",
          "mode": "100644",
          "type": "blob",
          "sha": "83d822893cda008b54618eb765928542f87376fe",
          "size": 974,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/83d822893cda008b54618eb765928542f87376fe"
      },
      {
          "path": "assets/images",
          "mode": "040000",
          "type": "tree",
          "sha": "37b500a3a276e080a33da11705aadc8b265af839",
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/trees/37b500a3a276e080a33da11705aadc8b265af839"
      },
      {
          "path": "assets/images/gitee-logo.svg",
          "mode": "100644",
          "type": "blob",
          "sha": "bf73d845766b52f21056b49f62e43ca119abd749",
          "size": 5170,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/bf73d845766b52f21056b49f62e43ca119abd749"
      },
      {
          "path": "index.html",
          "mode": "100644",
          "type": "blob",
          "sha": "092655da922316ccaf6d42cfb2443039d1af5ffd",
          "size": 1499,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/092655da922316ccaf6d42cfb2443039d1af5ffd"
      },
      {
          "path": "inline.31d246580a8a57e3373d.bundle.js",
          "mode": "100644",
          "type": "blob",
          "sha": "e034fb96e3ee225d76e92aa1e488a426cfddaa1f",
          "size": 1520,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/e034fb96e3ee225d76e92aa1e488a426cfddaa1f"
      },
      {
          "path": "main.d8565e43697fa9ec3bc4.bundle.js",
          "mode": "100644",
          "type": "blob",
          "sha": "36ed9f7e743d6ee63c98089d4d38798b82661db9",
          "size": 781536,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/36ed9f7e743d6ee63c98089d4d38798b82661db9"
      },
      {
          "path": "ngsw-manifest.json",
          "mode": "100644",
          "type": "blob",
          "sha": "e92b9d41c22104ca676730f26b381792d8d14d3e",
          "size": 2995,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/e92b9d41c22104ca676730f26b381792d8d14d3e"
      },
      {
          "path": "polyfills.eb4dbaa89de9c34fc3c4.bundle.js",
          "mode": "100644",
          "type": "blob",
          "sha": "468e7f828afb271248a5e975353c3ff300ac9ab2",
          "size": 60959,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/468e7f828afb271248a5e975353c3ff300ac9ab2"
      },
      {
          "path": "styles.0a58e1305b009a3c8ab7.bundle.css",
          "mode": "100644",
          "type": "blob",
          "sha": "e8bb6a4980529a2164953cf40a141e01a3511238",
          "size": 85022,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/e8bb6a4980529a2164953cf40a141e01a3511238"
      },
      {
          "path": "sw-register.d02a261dd2b31ab45198.bundle.js",
          "mode": "100644",
          "type": "blob",
          "sha": "5e466e9e41f9ae54d4fb5c20e8f9db303f2491c0",
          "size": 241,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/5e466e9e41f9ae54d4fb5c20e8f9db303f2491c0"
      },
      {
          "path": "worker-basic.min.js",
          "mode": "100644",
          "type": "blob",
          "sha": "a9e4fffce2fca36b3e0e2fd787058cb0509f4fa6",
          "size": 54001,
          "url": "https://gitee.com/api/v5/repos/yorkecao/yorkecao/git/blobs/a9e4fffce2fca36b3e0e2fd787058cb0509f4fa6"
      }
  ],
  "truncated": false
};

@Injectable()
export class GitProxyService {


  constructor(
    private http: HttpClient
  ) { }
  // Activity
  starred(owner: string, repo: string, access_token: string): void {
    let url = apiBasePath + '/v5/user/starred/' + owner + '/' + repo;
    let body = {
      access_token: access_token
    }
    this.http
      .put(url, body)
      .toPromise()
      .then();
  }

  // Git Data
  getBlob(owner: string, repo: string, sha: string, access_token?: string): Promise<any> {
    let url = apiBasePath + '/v5/repos/' + owner + '/' + repo + '/git/blobs/' + sha;
    let params = access_token
      ? { access_token: access_token }
      : {};
    return this.http
      .get(url, {
        params: params
      })
      .toPromise()
      .then(response => response);
  }

  getTree(owner: string, repo: string, sha: string, access_token?: string, recursive?: number): Promise<any> {
    // let url = apiBasePath + '/v5/repos/' + owner + '/' + repo + '/git/trees/' + sha;
    // let params = {};
    // if (access_token) {
    //   params = recursive
    //     ? { access_token: access_token, recursive: recursive }
    //     : { access_token: access_token };
    // } else if (recursive) {
    //   params = { recursive: recursive }
    // }
    // return this.http
    //   .get(url, {
    //     params: params
    //   })
    //   .toPromise()
    //   .then(response => response.json() as any);
    return Observable.of(releaseTree).toPromise();
  }

  // Issues
  simpleGetIssues(owner: string, repo: string, creator: string, page = 1, per_page = 20, state = 'open', sort = 'created', direction = 'desc'): Promise<any[]> {
    let url = apiBasePath + '/v5/repos/' + owner + '/' + repo + '/issues';
    let params = {
      creator: creator,
      page: '' + page,
      per_page: '' + per_page,
      state: state,
      sort: sort,
      direction: direction
    };
    return this.http
      .get<any[]>(url, {
        params: params
      })
      .toPromise();
  }

  createIssue(owner: string, repo: string, title: string, body: string, accessToken: string): Promise<any> {
    let url = apiBasePath + '/v5/repos/' + owner + '/' + repo + '/issues';
    let requestBody = {
      title: title,
      body: body,
      access_token: accessToken
    }
    return this.http
      .post(url, requestBody)
      .toPromise();
  }

  getIssue(owner: string, repo: string, number: string, accessToken?: string): Promise<any> {
    let url = apiBasePath + '/v5/repos/' + owner + '/' + repo + '/issues/' + number;
    let params = accessToken
      ? { access_token: accessToken }
      : {};
    return this.http
      .get(url, {
        params: params
      })
      .toPromise();
  }

  updateIssue(owner: string, repo: string, number: string, title: string, body: string, accessToken: string, state?: string): Promise<any> {
    let url = apiBasePath + '/v5/repos/' + owner + '/' + repo + '/issues/' + number;
    let requestBody = state
      ? { title: title, body: body, access_token: accessToken, state: state }
      : { title: title, body: body, access_token: accessToken };
    return this.http
      .patch(url, requestBody)
      .toPromise();
  }

  // Miscellaneous
  markdownText(text: string, access_token?: string): Promise<string> {
    let url = apiBasePath + '/v5/markdown';
    let body = access_token
      ? { text: text, access_token: access_token }
      : { text: text };
    return this.http
      .post(url, body, {
        responseType: 'text'
      })
      .toPromise();
  }

  // Repositories
  getBranches(owner: string, repo: string, access_token?: string): Promise<any[]> {
    let url = apiBasePath + '/v5/repos/' + owner + '/' + repo + '/branches';
    let params = access_token
      ? { access_token: access_token }
      : {}
    return this.http
      .get<any[]>(url, {
        params: params
      })
      .toPromise();
  }

  simpleGetCommits(owner: string, repo: string, sha: string): Promise<any[]> {
    let url = apiBasePath + '/v5/repos/' + owner + '/' + repo + '/commits';
    let params = {
      sha: sha
    };
    return this.http
      .get<any[]>(url, {
        params: params
      })
      .toPromise();
  }

  getReadme(owner: string, repo: string, ref?: string, access_token?: string): Promise<any> {
    let url = apiBasePath + '/v5/repos/' + owner + '/' + repo + '/readme';
    let params = {};
    if (ref) {
      params = access_token
        ? { ref: ref, access_token: access_token }
        : { ref: ref };
    } else if (access_token) {
      params = { access_token: access_token };
    }
    return this.http
      .get(url, {
        params: params
      })
      .toPromise();
  }

  getDirContents(owner: string, repo: string, path?: string, ref?: string, access_token?: string): Promise<any[]> {
    let url = path
      ? apiBasePath + '/v5/repos/' + owner + '/' + repo + '/contents' + path
      : apiBasePath + '/v5/repos/' + owner + '/' + repo + '/contents';

    let params = {};
    if (ref) {
      params = access_token
        ? { ref: ref, access_token: access_token }
        : { ref: ref };
    } else if (access_token) {
      params = { access_token: access_token };
    }

    return this.http
      .get<any[]>(url, {
        params: params
      })
      .toPromise();
  }

  getFileContent(owner: string, repo: string, path: string, ref?: string, access_token?: string): Promise<any> {
    let url = apiBasePath + '/v5/repos/' + owner + '/' + repo + '/contents' + path;

    let params = {};
    if (ref) {
      params = access_token
        ? { ref: ref, access_token: access_token }
        : { ref: ref };
    } else if (access_token) {
      params = { access_token: access_token };
    }

    return this.http
      .get(url, {
        params: params
      })
      .toPromise();
  }

  updateFile(owner: string, repo: string, path: string, content: string, sha: string, message: string, access_token: string, branch?: string): Promise<any> {
    let url = apiBasePath + '/v5/repos/' + owner + '/' + repo + '/contents' + path;
    let body = branch
      ? { content: content, sha: sha, message: message, access_token: access_token, branch: branch }
      : { content: content, sha: sha, message: message, access_token: access_token };
    return this.http
      .put(url, body)
      .toPromise();
  }

  createFile(owner: string, repo: string, path: string, content: string, message: string, access_token: string, branch?: string): Promise<any> {
    let url = apiBasePath + '/v5/repos/' + owner + '/' + repo + '/contents' + path;
    let body = branch
      ? { content: content, message: message, access_token: access_token, branch: branch }
      : { content: content, message: message, access_token: access_token };
    return this.http
      .post(url, body)
      .toPromise();
  }

  deleteFile(owner: string, repo: string, path: string, sha: string, message: string, access_token: string, branch?: string): Promise<any> {
    let url = apiBasePath + '/v5/repos/' + owner + '/' + repo + '/contents' + path;
    let params = branch
      ? { sha: sha, message: message, access_token: access_token, branch: branch }
      : { sha: sha, message: message, access_token: access_token };
    return this.http
      .delete(url, {
        params: params
      })
      .toPromise();
  }

  getRepo(owner: string, repo: string, access_token?: string): Promise<any> {
    let url = apiBasePath + '/v5/repos/' + owner + '/' + repo;
    let params = access_token
      ? { access_token: access_token }
      : {};
    return this.http
      .get(url, {
        params: params
      })
      .toPromise();
  }

  getTags(owner: string, repo: string, access_token?: string): Promise<any[]> {
    let url = apiBasePath + '/v5/repos/' + owner + '/' + repo + '/tags';
    let params = access_token
      ? { access_token: access_token }
      : {};
    return this.http
      .get<any[]>(url, {
        params: params
      })
      .toPromise();
  }

  // users
  getAuthUserInfo(access_token: string): Promise<any> {
    let url = apiBasePath + '/v5/user';
    let params = {
      access_token: access_token
    }
    return this.http
      .get(url, {
        params: params
      })
      .toPromise();
  }
}
