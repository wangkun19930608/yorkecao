import { Component, ViewChild, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { OverlayContainer } from '@angular/cdk/overlay';

import { ConfService } from './conf.service';
import { AuthService } from './auth.service';
import { AboutMeComponent } from './about-me/about-me.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title: string;
  isDark: boolean;

  constructor(
    private overlayContainer: OverlayContainer,
    private router: Router,
    public snackBar: MatSnackBar,
    public dialog: MatDialog,
    public confService: ConfService,
    public authService: AuthService
  ) { }

  ngOnInit() {
    this.confService.initConfig();
    // this.title = this.confService.config.repo;
    this.title = 'YK-DOC';
    this.isDark = false;
  }

  ifYkdoc(): boolean {
    return this.confService.config.owner == 'yorkecao' && this.confService.config.repo == 'yorkecao';
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(AboutMeComponent, {
      minWidth: '25vw',
      minHeight: '30vh'
    });
  }

  toggleTheme(): void {
    if (!this.isDark) {
      this.overlayContainer.getContainerElement().classList.add('unicorn-dark-theme');
    } else {
      this.overlayContainer.getContainerElement().classList.remove('unicorn-dark-theme');
    }
  }

  logout(): void {
    this.authService.logout();
    this.snackBar.open("已注销", "", {
      duration: 600,
    });
  }
}

