# 什么是 YK-DOC

YK-DOC 是一个用 Angular 编写的，基于码云 Pages 服务的在线文档系统。

点击链接预览：[YK-DOC 主页](https://yorkecao.gitee.io/)

![桌面预览](https://gitee.com/uploads/images/2017/1109/173304_66e48dcd_585591.png "桌面预览.png")

![手机预览](https://gitee.com/uploads/images/2017/1109/173328_364372be_585591.jpeg "手机预览.jpg")

## 特点

- 渐进式应用（PWA）

  升级为 Progressive Web App，可离线使用，更接近原生应用体验。

- 基于码云 Pages + Open API 服务

  这是一个纯前端应用，部署在码云 Pages 服务中使用。

  所有的文档也都托管在码云中，通过其 Open API 获取内容。理论上自带版本管理功能。

- 支持 markdown

  原生支持 markdown 格式，在线解析。

- 急速部署，轻松使用

  通过“初始化器”急速部署，即刻使用。

  整个部署及使用过程全部在线完成，无需 Git 工具。

- Material Desion

  组件来自 Angular Material。

  官方 Material Desion，在桌面端移动端都有优秀的视觉效果，自带 light、dark 两套皮肤。

- 面向现代浏览器

  基于最新 Angular（5.0.0） 开发，并计划跟进 Angular 版本。




## 使用场景

- 为项目编写使用手册

  项目使用复杂，README 满足不了？使用 YK-DOC 快速部署基于 Pages 的帮助手册页面，更好地展示你的使用手册。

- 个人博客

  想要一个个人博客？使用 YK-DOC 快速搭建个人博客，在线发布原创博文和动态。

- 记录笔记

  使用基于 Git 的 YK-DOC 作为在线笔记本，自带版本管理，记录不会丢失，手机也可以方便地阅读和使用。




## 其他

这是一个我在学习 Angular 的过程中编写的项目，里面使用了诸如：服务、管道、路由、路由守卫、事件处理、表单验证等特性。如果你也在学习 Angular，希望这个项目可以给你提供一些参考。

如果你发现该项目有任何的不足之处，谢谢指正！
